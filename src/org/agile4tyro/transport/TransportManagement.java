package org.agile4tyro.transport;

import java.util.LinkedList;
import java.util.List;

public class TransportManagement {
    private List availableVehicles = new LinkedList();
    private int totalSeats;
    private static final TransportManagement management = new TransportManagement();

    public static TransportManagement addAvailableVehicles(Object... vehicles){
        for(Object vehicle : vehicles){
            management.availableVehicles.add(vehicle);
            management.totalSeats += ((AutoMobile) vehicle).availableSeats();
        }
        return management;
    }

    public void allocateSeats(int numberOfSeats){
        //Subtract from totalSeats
    }

    public void deallocateSeats(int numberOfSeats){
        //add to totalSeats
    }

    public int getAvailableSeats(){
        return totalSeats;
    }
}
