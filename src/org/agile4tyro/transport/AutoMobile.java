package org.agile4tyro.transport;

/**
 * Created by sheldon on 9/8/15.
 */
public interface AutoMobile {
    public int availableSeats();
}
