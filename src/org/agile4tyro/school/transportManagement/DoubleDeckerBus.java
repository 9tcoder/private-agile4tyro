package org.agile4tyro.school.transportManagement;

import org.agile4tyro.transport.AutoMobile;

/**
 * Created by sheldon on 9/8/15.
 */
public class DoubleDeckerBus implements AutoMobile{
    private int CAPACITY;

    public DoubleDeckerBus(int availableSeats) {
        CAPACITY = availableSeats;
    }
    @Override
    public int availableSeats() {
        return CAPACITY;
    }
}
