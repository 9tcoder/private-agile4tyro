package org.agile4tyro.school.transportManagement;
import org.agile4tyro.transport.AutoMobile;

/**
 * Created by 9tcoder on 9/8/15.
 */

public class NannyVan implements AutoMobile {
    private int CAPACITY;

    public NannyVan(int availableSeats) {
        CAPACITY = availableSeats;
    }

    @Override
    public int availableSeats() {
        return CAPACITY;
    }
}
