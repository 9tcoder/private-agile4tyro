package org.agile4tyro.school.transportManagement;

import org.agile4tyro.transport.TransportManagement;

/**
 * Created by 9tcoder on 9/8/15.
 */
public class TransportDepartment {

    public static void main(String[] args) {

        NannyVan van1 = new NannyVan(15);
        NannyVan van2 = new NannyVan(13);
        DoubleDeckerBus deckerBus1 = new DoubleDeckerBus(75);
        DoubleDeckerBus deckerBus2 = new DoubleDeckerBus(80);

        TransportManagement management = TransportManagement.addAvailableVehicles(van1, van2, deckerBus1, deckerBus2);
        System.out.println("No. of available seats : "+management.getAvailableSeats());
    }
}
