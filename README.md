# README #
Here I am trying to give you an overview of Java Generics. The code given is the java application. You need JDK 1.5 or above to work with this code. 

### About Code ###
The scenario which is given here is about School's Transport management. 
Here is the explain of all packages : 
####org.agile4tyro.transport####
There is some code which is assumed that it is provided as an API or say third party code. So, treat that code as you just have class files. Even though you can change :).

>Classes :
 
>* AutoMobile
>* TransportManagement 
####org.agile4tyro.school.transportManagement####
This is the code which uses the _org.agile4tyro.transport_ classes. We can add new type of Vehicles in this package and use them by using _TransportDepartment_ class. _TransportDepartment_ uses the API _org.agile4tyro.transport.TransportManagement_.

>Classes : 

>*  TransportDepartment
>*  NannyVan
>*  DoubleDeckerBus